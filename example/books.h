#ifndef __BOOKS_H__
#define __BOOKS_H__

#include "book.h"
#include "cvector.h"

CVECTOR_GENERATE_HEADER(book_ptr)
typedef cvector(book_ptr) books_t;

books_t *books_new(size_t size);
books_t *books_add(books_t *books, const char *author, const char *title, int id);
void books_del(books_t *books);
void books_dump(books_t *books);

#endif /* __BOOKS_H__ */
