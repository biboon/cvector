#include <stdio.h>

#include "books.h"

int main(int argc, char *argv[])
{
    books_t *books;

    (void)argc;
    (void)argv;

    books = books_new(5);
    books = books_add(books, "zep", "titeuf", 12);
    books = books_add(books, "dostoievski", "joueur", 42);
    books = books_add(books, "tamer", "lashov", 777);

    books_dump(books);
    books_del(books);

    return 0;
}
