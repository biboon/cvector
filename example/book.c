#include "book.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct book_s
{
    int id;
    char *author;
    char *title;
};

book_t *book_new(const char *author, const char *title, int id)
{
    book_t *book;

    book = malloc(sizeof(*book));
    if (book == NULL)
        return NULL;

    book->author = strdup(author);
    book->title  = strdup(title);
    book->id     = id;

    return book;
}

void book_del(book_t *book)
{
    if (book == NULL)
        return;

    free(book->author);
    free(book->title);
    free(book);
}

void book_dump(book_t *book)
{
    printf("%s by %s (#%d)\n", book->title, book->author, book->id);
}
