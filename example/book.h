#ifndef __BOOK_H_
#define __BOOK_H_

typedef struct book_s book_t, *book_ptr;

book_t *book_new(const char *author, const char *title, int id);
void book_del(book_t *book);
void book_dump(book_t *book);

#endif /* __BOOK_H_ */
