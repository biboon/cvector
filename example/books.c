#include "books.h"


CVECTOR_GENERATE_SOURCE(book_ptr)


books_t *books_new(size_t size)
{
    return cvector_make(book_ptr, size);
}

books_t *books_add(books_t *books, const char *author, const char *title, int id)
{
    return cvector_push(book_ptr, books, book_new(author, title, id));
}

void books_del(books_t *books)
{
    cvector_foreach(book_ptr, books, book_del);
    cvector_delete(book_ptr, books);
}

void books_dump(books_t *books)
{
    cvector_foreach(book_ptr, books, book_dump);
}
