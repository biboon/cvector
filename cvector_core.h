#ifndef __CVECTOR_CORE_H__
#define __CVECTOR_CORE_H__

#include <stdbool.h>
#include <stdlib.h>


#define CVECTOR_TYPE(type)   cvector_ ##type ##_t
#define CVECTOR_STRUCT(type) cvector_ ##type ##_s

#define CVECTOR_FUNCTION_DELETE(type)      cvector_ ##type ##_delete
#define CVECTOR_FUNCTION_MAKE(type)        cvector_ ##type ##_make
#define CVECTOR_FUNCTION_FOREACH(type)     cvector_ ##type ##_foreach
#define CVECTOR_FUNCTION_GROW(type)        cvector_ ##type ##_grow
#define CVECTOR_FUNCTION_SHRINK(type)      cvector_ ##type ##_shrink
#define CVECTOR_FUNCTION_PUSH(type)        cvector_ ##type ##_push
#define CVECTOR_FUNCTION_POP(type)         cvector_ ##type ##_pop
#define CVECTOR_FUNCTION_ASSIGN(type)      cvector_ ##type ##_assign
#define CVECTOR_FUNCTION_CLEAR(type)       cvector_ ##type ##_clear


/* Define the growth strategy when expanding a vector */
#define CVECTOR_GROW_CAPACITY(cap) ((cap) * 2)


#define CVECTOR_DEFINE_STRUCT(type) \
struct CVECTOR_STRUCT(type) \
{ \
    /* Using const qualifiers to prohibit the following : \
     *  - xxx->cap = ...; \
     *  - xxx->len = ...; \
     *  - xxx->at  = ...; -- forbidden because `at' is a flexible array \
     */ \
    const size_t cap, len; \
    type at[]; \
};

#define CVECTOR_DEFINE_TYPE(type) \
typedef struct CVECTOR_STRUCT(type) CVECTOR_TYPE(type);


#define CVECTOR_DECLARE_FUNCTIONS(type) \
    CVECTOR_DECLARE_FUNCTION_DELETE(type) \
    CVECTOR_DECLARE_FUNCTION_MAKE(type) \
    CVECTOR_DECLARE_FUNCTION_FOREACH(type) \
    CVECTOR_DECLARE_FUNCTION_GROW(type) \
    CVECTOR_DECLARE_FUNCTION_SHRINK(type) \
    CVECTOR_DECLARE_FUNCTION_PUSH(type) \
    CVECTOR_DECLARE_FUNCTION_POP(type) \
    CVECTOR_DECLARE_FUNCTION_ASSIGN(type) \
    CVECTOR_DECLARE_FUNCTION_CLEAR(type) \

#define CVECTOR_DEFINE_FUNCTIONS(type) \
    CVECTOR_DEFINE_FUNCTION_DELETE(type) \
    CVECTOR_DEFINE_FUNCTION_MAKE(type) \
    CVECTOR_DEFINE_FUNCTION_FOREACH(type) \
    CVECTOR_DEFINE_FUNCTION_GROW(type) \
    CVECTOR_DEFINE_FUNCTION_SHRINK(type) \
    CVECTOR_DEFINE_FUNCTION_PUSH(type) \
    CVECTOR_DEFINE_FUNCTION_POP(type) \
    CVECTOR_DEFINE_FUNCTION_ASSIGN(type) \
    CVECTOR_DEFINE_FUNCTION_CLEAR(type) \


#define CVECTOR_DECLARE_FUNCTION_DELETE(type) \
void CVECTOR_FUNCTION_DELETE(type)(CVECTOR_TYPE(type) *vec);

#define CVECTOR_DEFINE_FUNCTION_DELETE(type) \
void CVECTOR_FUNCTION_DELETE(type)(CVECTOR_TYPE(type) *vec) \
{ \
    free(vec); \
}


#define CVECTOR_DECLARE_FUNCTION_MAKE(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_MAKE(type)(size_t size);

#define CVECTOR_DEFINE_FUNCTION_MAKE(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_MAKE(type)(size_t size) \
{ \
    CVECTOR_TYPE(type) *vec; \
\
    vec = malloc(sizeof(*vec) + size * sizeof(type)); \
    if (vec == NULL) \
        return NULL; \
\
    *(size_t *)&vec->cap = size; \
    *(size_t *)&vec->len = 0; \
\
    return vec; \
}


#define CVECTOR_DECLARE_FUNCTION_FOREACH(type) \
void CVECTOR_FUNCTION_FOREACH(type)(CVECTOR_TYPE(type) *vec, void (*f)(type));

#define CVECTOR_DEFINE_FUNCTION_FOREACH(type) \
void CVECTOR_FUNCTION_FOREACH(type)(CVECTOR_TYPE(type) *vec, void (*f)(type)) \
{ \
    if (vec != NULL) \
    { \
        for (size_t i = 0; i < vec->len; i++) \
            f(vec->at[i]); \
    } \
}


#define CVECTOR_DECLARE_FUNCTION_GROW(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_GROW(type)(CVECTOR_TYPE(type) *vec, size_t size);

#define CVECTOR_DEFINE_FUNCTION_GROW(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_GROW(type)(CVECTOR_TYPE(type) *vec, size_t size) \
{ \
    CVECTOR_TYPE(type) *new_vec; \
    size_t cap; \
\
    if (vec == NULL) \
        return NULL; \
\
    if (size <= vec->cap) \
        return vec; \
\
    cap = (vec->cap != 0) ? vec->cap : size; \
    while (cap < size) cap = CVECTOR_GROW_CAPACITY(cap); \
\
    new_vec = realloc(vec, sizeof(*vec) + cap * sizeof(type)); \
    if (new_vec == NULL) \
        return NULL; \
\
    *(size_t *)&new_vec->cap = cap; \
\
    return new_vec; \
}


#define CVECTOR_DECLARE_FUNCTION_SHRINK(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_SHRINK(type)(CVECTOR_TYPE(type) *vec);

#define CVECTOR_DEFINE_FUNCTION_SHRINK(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_SHRINK(type)(CVECTOR_TYPE(type) *vec) \
{ \
    CVECTOR_TYPE(type) *new_vec; \
\
    if (vec == NULL) \
        return NULL; \
\
    new_vec = realloc(vec, sizeof(*vec) + vec->len * sizeof(type)); \
    if (new_vec == NULL) \
        return NULL; \
\
    *(size_t *)&new_vec->cap = new_vec->len; \
\
    return new_vec; \
}


#define CVECTOR_DECLARE_FUNCTION_PUSH(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_PUSH(type)(CVECTOR_TYPE(type) *vec, type val);

#define CVECTOR_DEFINE_FUNCTION_PUSH(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_PUSH(type)(CVECTOR_TYPE(type) *vec, type val) \
{ \
    CVECTOR_TYPE(type) *new_vec; \
\
    if (vec == NULL) \
        return NULL; \
\
    new_vec = CVECTOR_FUNCTION_GROW(type)(vec, vec->len + 1); \
    if (new_vec == NULL) \
        return NULL; \
\
    new_vec->at[new_vec->len] = val; \
    *(size_t *)&new_vec->len = new_vec->len + 1; \
\
    return new_vec; \
}


#define CVECTOR_DECLARE_FUNCTION_POP(type) \
bool CVECTOR_FUNCTION_POP(type)(CVECTOR_TYPE(type) *vec, type *val);

#define CVECTOR_DEFINE_FUNCTION_POP(type) \
bool CVECTOR_FUNCTION_POP(type)(CVECTOR_TYPE(type) *vec, type *val) \
{ \
    if (vec == NULL || vec->len == 0) \
        return false; \
\
    *(size_t *)&vec->len = vec->len - 1; \
    *val = vec->at[vec->len]; \
\
    return true; \
\
}


#define CVECTOR_DECLARE_FUNCTION_ASSIGN(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_ASSIGN(type)(CVECTOR_TYPE(type) *vec, size_t size, type val);

#define CVECTOR_DEFINE_FUNCTION_ASSIGN(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_ASSIGN(type)(CVECTOR_TYPE(type) *vec, size_t size, type val) \
{ \
    CVECTOR_TYPE(type) *new_vec; \
\
    if (vec == NULL) \
        return NULL; \
\
    new_vec = CVECTOR_FUNCTION_GROW(type)(vec, size); \
    if (new_vec == NULL) \
        return NULL; \
\
    for (size_t i = 0; i < size; i++) \
        new_vec->at[i] = val; \
\
    *(size_t *)&new_vec->len = size; \
\
    return new_vec; \
}


#define CVECTOR_DECLARE_FUNCTION_CLEAR(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_CLEAR(type)(CVECTOR_TYPE(type) *vec);

#define CVECTOR_DEFINE_FUNCTION_CLEAR(type) \
CVECTOR_TYPE(type) *CVECTOR_FUNCTION_CLEAR(type)(CVECTOR_TYPE(type) *vec) \
{ \
    if (vec == NULL) \
        return NULL; \
\
    *(size_t *)&vec->len = 0; \
\
    return vec; \
}

#endif /* __CVECTOR_CORE_H__ */
