ifndef VERBOSE
.SILENT:
define tell
	echo -e "\033[0;36m$(1)\033[0m"
endef
endif


CFLAGS := -std=c99 -D_DEFAULT_SOURCE -Wall -Wextra
CFLAGS += -Wshadow -Wpointer-arith -Wvla -Wdeclaration-after-statement
CFLAGS += -Wconversion -Winit-self -Wold-style-definition
CFLAGS += -Wmissing-field-initializers -Wno-unknown-pragmas -Wundef
CFLAGS += -Wcast-align -Wstrict-prototypes -Wmissing-prototypes
CFLAGS += -Wwrite-strings -Wswitch-default -Wunreachable-code


default: tests

tests: $(patsubst tests/%.c,bin/tests/%.out,$(wildcard tests/*.c))
	$(call tell,"Running tests...")
	$(foreach f,$^,./$(f);)

tests-memcheck: $(patsubst tests/%.c,bin/tests/%.out,$(wildcard tests/*.c))
	$(call tell,"Running tests \(memcheck\)...")
	$(foreach f,$^,valgrind --quiet --tool=memcheck --leak-check=full ./$(f);)

example: bin/example/books.out
	./bin/example/books.out

clean:
	rm -rf bin build


bin/tests/%.out: tests/%.c tests/cute.h cvector.h cvector_core.h
	@mkdir -p $(@D)
	$(call tell,"Building $@")
	$(CC) $(CFLAGS) -Wno-missing-prototypes -o $@ $< -Itest/ -I./

bin/example/books.out: build/example/main.o build/example/book.o build/example/books.o
	@mkdir -p $(@D)
	$(CC) $(LDFLAGS) -o $@ $^


.PRECIOUS: build/example/%.o

build/example/%.o: example/%.c example/%.h cvector.h cvector_core.h
	@mkdir -p $(@D)
	$(call tell,"Building object $@")
	$(CC) $(CFLAGS) -Wno-missing-prototypes -c -o $@ $< -I./

build/example/main.o: example/main.c
	@mkdir -p $(@D)
	$(call tell,"Building object $@")
	$(CC) $(CFLAGS) -Wno-missing-prototypes -c -o $@ $< -I./
