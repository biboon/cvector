#ifndef __CVECTOR_H__
#define __CVECTOR_H__

#include "cvector_core.h"


#define CVECTOR_GENERATE_HEADER(type) \
    CVECTOR_DEFINE_STRUCT(type) \
    CVECTOR_DEFINE_TYPE(type) \
    CVECTOR_DECLARE_FUNCTIONS(type)

#define CVECTOR_GENERATE_SOURCE(type) \
    CVECTOR_DEFINE_FUNCTIONS(type)


#define cvector(type) CVECTOR_TYPE(type)

#define cvector_delete(type, vec)                CVECTOR_FUNCTION_DELETE(type)(vec)
#define cvector_make(type, size)                 CVECTOR_FUNCTION_MAKE(type)(size)
#define cvector_foreach(type, vec, func)         CVECTOR_FUNCTION_FOREACH(type)(vec, func)
#define cvector_grow(type, vec, size)            CVECTOR_FUNCTION_GROW(type)(vec, size)
#define cvector_shrink(type, vec)                CVECTOR_FUNCTION_SHRINK(type)(vec)
#define cvector_push(type, vec, val)             CVECTOR_FUNCTION_PUSH(type)(vec, val)
#define cvector_pop(type, vec, pval)             CVECTOR_FUNCTION_POP(type)(vec, pval)
#define cvector_assign(type, vec, size, val)     CVECTOR_FUNCTION_ASSIGN(type)(vec, size, val)
#define cvector_clear(type, vec)                 CVECTOR_FUNCTION_CLEAR(type)(vec)

#endif /* __CVECTOR_H__ */
