#include "cvector.h"
#include "cute.h"

/* ------------ */

CVECTOR_GENERATE_HEADER(double)
CVECTOR_GENERATE_SOURCE(double)

/* ------------ */

void setUp(void) {}
void tearDown(void) {}
void suiteSetUp(void) {}
void suiteTearDown(void) {}


void push_pop_null(void)
{
    TEST_ASSERT_NULL(cvector_push(double, NULL, 1.618));
    TEST_ASSERT_FALSE(cvector_pop(double, NULL, NULL));
}

void sub_push(size_t make, size_t assign, size_t cap)
{
    cvector(double) *ori, *vec;

    ori = cvector_make(double, make);
    ori = cvector_assign(double, ori, assign, 3.14);
    vec = cvector_push(double, ori, 1.618);

    if (assign < make)
        TEST_ASSERT_PTR_EQ(ori, vec);

    TEST_ASSERT_ULONG_OP(==, cap, vec->cap);
    TEST_ASSERT_ULONG_OP(==, assign + 1, vec->len);
    TEST_ASSERT_DOUBLE_OP(==, 1.618, vec->at[vec->len - 1]);

    cvector_delete(double, vec);
}

void push_to_zero_cap(void) { sub_push(0, 0, 1); }
void push_to_zero_len(void) { sub_push(10, 0, 10); }
void push_to_non_empty(void) { sub_push(10, 7, 10); }
void push_to_full(void) { sub_push(10, 10, 20); }

void sub_pop(size_t make, size_t assign, size_t cap)
{
    cvector(double) *vec;
    double val;
    bool ok;

    vec = cvector_make(double, make);
    vec = cvector_assign(double, vec, assign, 3.14);
    ok  = cvector_pop(double, vec, &val);

    if (assign > 1)
    {
        TEST_ASSERT_TRUE(ok);
        TEST_ASSERT_ULONG_OP(==, cap, vec->cap);
        TEST_ASSERT_ULONG_OP(==, assign - 1, vec->len);
        TEST_ASSERT_DOUBLE_OP(==, 3.14, val);
    }
    else
    {
        TEST_ASSERT_FALSE(ok);
        TEST_ASSERT_ULONG_OP(==, cap, vec->cap);
        TEST_ASSERT_ULONG_OP(==, 0UL, vec->len);
    }

    cvector_delete(double, vec);
}

void pop_from_zero_cap(void) { sub_pop(0, 0, 0); }
void pop_from_zero_len(void) { sub_pop(10, 0, 10); }
void pop_from_non_empty(void) { sub_pop(10, 7, 10); }
void pop_from_full(void) { sub_pop(10, 10, 10); }


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(push_pop_null);

    TEST_RUN(push_to_zero_cap);
    TEST_RUN(push_to_zero_len);
    TEST_RUN(push_to_non_empty);
    TEST_RUN(push_to_full);

    TEST_RUN(pop_from_zero_cap);
    TEST_RUN(pop_from_zero_len);
    TEST_RUN(pop_from_non_empty);
    TEST_RUN(pop_from_full);

    return TEST_END();
}
