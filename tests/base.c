#include "cvector.h"
#include "cute.h"

/* ------------ */

CVECTOR_GENERATE_HEADER(double)
CVECTOR_GENERATE_SOURCE(double)

/* ------------ */

void setUp(void) {}
void tearDown(void) {}
void suiteSetUp(void) {}
void suiteTearDown(void) {}


void delete_null(void)
{
    cvector_delete(double, NULL);
}

void make_zero(void)
{
    cvector(double) *vec = cvector_make(double, 0);

    TEST_ASSERT_NOT_NULL(vec);
    TEST_ASSERT_ULONG_OP(==, 0UL, vec->len);
    TEST_ASSERT_ULONG_OP(==, 0UL, vec->cap);

    cvector_delete(double, vec);
}

void make_non_zero(void)
{
    cvector(double) *vec = cvector_make(double, 42);

    TEST_ASSERT_NOT_NULL(vec);
    TEST_ASSERT_ULONG_OP(==,  0UL, vec->len);
    TEST_ASSERT_ULONG_OP(==, 42UL, vec->cap);

    cvector_delete(double, vec);
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(delete_null);
    TEST_RUN(make_zero);
    TEST_RUN(make_non_zero);

    return TEST_END();
}
