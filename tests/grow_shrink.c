#include "cvector.h"
#include "cute.h"

/* ------------ */

CVECTOR_GENERATE_HEADER(double)
CVECTOR_GENERATE_SOURCE(double)

/* ------------ */

void setUp(void) {}
void tearDown(void) {}
void suiteSetUp(void) {}
void suiteTearDown(void) {}


void grow_shrink_null(void)
{
    TEST_ASSERT_NULL(cvector_grow(double, NULL, 0));
    TEST_ASSERT_NULL(cvector_grow(double, NULL, 8));
    TEST_ASSERT_NULL(cvector_shrink(double, NULL));
}

void sub_grow(size_t make, size_t grow, size_t cap)
{
    cvector(double) *ori = cvector_make(double, make);
    cvector(double) *vec = cvector_grow(double, ori, grow);

    if (grow <= make)
        TEST_ASSERT_PTR_EQ(ori, vec);

    TEST_ASSERT_ULONG_OP(==, cap, vec->cap);
    TEST_ASSERT_ULONG_OP(==, 0UL, vec->len);

    /* Make sure the allocated memory is writable */
    while (vec->len < vec->cap) cvector_push(double, vec, 3.14);

    cvector_delete(double, vec);
}

void grow_zero_to_zero(void) { sub_grow(0,  0,  0); }
void grow_zero_to_more(void) { sub_grow(0, 42, 42); }
void grow_non_zero_to_zero(void) { sub_grow(10,  0, 10); }
void grow_non_zero_to_less(void) { sub_grow(10,  7, 10); }
void grow_non_zero_to_same(void) { sub_grow(10, 10, 10); }
void grow_non_zero_to_more(void) { sub_grow(10, 13, 20); }
void grow_non_zero_to_next(void) { sub_grow(10, 20, 20); }
void grow_non_zero_to_much(void) { sub_grow(10, 42, 80); }

void grow_test_content(void)
{
    cvector(double) *vec;

    vec = cvector_make(double, 0);
    vec = cvector_assign(double, vec, 10, 3.14);
    vec = cvector_grow(double, vec, 42);

    TEST_ASSERT_ULONG_OP(==, 10UL, vec->len);
    for (size_t i = 0; i < vec->len; i++)
        TEST_ASSERT_DOUBLE_OP(==, 3.14, vec->at[i]);

    cvector_delete(double, vec);
}

void sub_shrink(size_t make, size_t assign)
{
    cvector(double) *vec;

    vec = cvector_make(double, make);
    vec = cvector_assign(double, vec, assign, 3.14);
    vec = cvector_shrink(double, vec);

    TEST_ASSERT_ULONG_OP(==, assign, vec->cap);
    TEST_ASSERT_ULONG_OP(==, assign, vec->len);

    if (assign > 1)
    {
        TEST_ASSERT_DOUBLE_OP(==, 3.14, vec->at[0]);
        TEST_ASSERT_DOUBLE_OP(==, 3.14, vec->at[vec->len - 1]);
    }

    cvector_delete(double, vec);
}

void shrink_zero_cap(void) { sub_shrink( 0, 0); }
void shrink_zero_len(void) { sub_shrink(10, 0); }
void shrink_non_empty(void) { sub_shrink(10, 7); }

void shrink_test_content(void)
{
    cvector(double) *vec;

    vec = cvector_make(double, 0);
    vec = cvector_assign(double, vec, 42, 3.14);
    vec = cvector_assign(double, vec, 10, 1.618);
    vec = cvector_shrink(double, vec);

    TEST_ASSERT_ULONG_OP(==, 10UL, vec->len);
    for (size_t i = 0; i < vec->len; i++)
        TEST_ASSERT_DOUBLE_OP(==, 1.618, vec->at[i]);

    cvector_delete(double, vec);
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(grow_shrink_null);

    TEST_RUN(grow_zero_to_zero);
    TEST_RUN(grow_zero_to_more);
    TEST_RUN(grow_non_zero_to_zero);
    TEST_RUN(grow_non_zero_to_less);
    TEST_RUN(grow_non_zero_to_same);
    TEST_RUN(grow_non_zero_to_more);
    TEST_RUN(grow_non_zero_to_next);
    TEST_RUN(grow_non_zero_to_much);
    TEST_RUN(grow_test_content);

    TEST_RUN(shrink_zero_cap);
    TEST_RUN(shrink_zero_len);
    TEST_RUN(shrink_non_empty);
    TEST_RUN(shrink_test_content);

    return TEST_END();
}
