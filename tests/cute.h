#ifndef _CUTE_H_
#define _CUTE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>


static void setUp(void);
static void tearDown(void);
static void suiteSetUp(void);
static void suiteTearDown(void);


#define TEST_IGNORE()                             CUTE_IGNORE()

#define TEST_ASSERT(condition)                    CUTE_ASSERT( (condition), "`%s'", #condition)
#define TEST_ASSERT_TRUE(condition)               CUTE_ASSERT( (condition), "want `%s' TRUE" , #condition)
#define TEST_ASSERT_FALSE(condition)              CUTE_ASSERT(!(condition), "want `%s' FALSE", #condition)
#define TEST_ASSERT_NOT_NULL(ptr)                 CUTE_ASSERT((ptr) != NULL, "want %s != NULL", #ptr)
#define TEST_ASSERT_NULL(ptr)                     CUTE_ASSERT((ptr) == NULL, "want %s == NULL", #ptr)
#define TEST_ASSERT_MEM_EQ(exp, act, len)         CUTE_ASSERT(!memcmp(exp, act, len), "want *%s == *%s", #exp, #act)
#define TEST_ASSERT_STR_EQ(exp, act)              CUTE_ASSERT(!strcmp(exp, act), "want %s == \"%s\" got \"%s\"", #exp, exp, act)
#define TEST_ASSERT_PTR_EQ(exp, act)              CUTE_ASSERT((exp) == (act), "want %s == %p got %p", #exp, exp, act)

#define TEST_ASSERT_CHAR_OP(op, exp, act)         CUTE_ASSERT_OP(op, exp, act, "'%c'")
#define TEST_ASSERT_INT_OP(op, exp, act)          CUTE_ASSERT_OP(op, exp, act, "%d"  )
#define TEST_ASSERT_UINT_OP(op, exp, act)         CUTE_ASSERT_OP(op, exp, act, "%u"  )
#define TEST_ASSERT_LONG_OP(op, exp, act)         CUTE_ASSERT_OP(op, exp, act, "%ld" )
#define TEST_ASSERT_ULONG_OP(op, exp, act)        CUTE_ASSERT_OP(op, exp, act, "%lu" )
#define TEST_ASSERT_FLOAT_OP(op, exp, act)        CUTE_ASSERT_OP(op, exp, act, "%f"  )
#define TEST_ASSERT_DOUBLE_OP(op, exp, act)       CUTE_ASSERT_OP(op, exp, act, "%lf" )
#define TEST_ASSERT_HEX_OP(op, exp, act)          CUTE_ASSERT_OP(op, exp, act, "%#x" )


#define TEST_RUN(test) cute_execute(test, #test)
#define TEST_BEGIN()   cute_initiate(__FILE__)
#define TEST_END()     cute_conclude(__FILE__)


/* ------------------ */
/* Internal functions */
/* ------------------ */

enum { CUTE_SUCCESS, CUTE_FAILURE, CUTE_IGNORED };
static struct timeval cute_tick;
static int cute_result;
static int cute_successes, cute_failures, cute_ignores;

static void cute_initiate(const char *file)
{
    printf("Running test file %s...\n", file);
    gettimeofday(&cute_tick, NULL);
    suiteSetUp();
}

static void cute_execute(void (*test_func)(void), const char *test_name)
{
    const char *status;

    setUp();
    test_func();
    tearDown();

    switch (cute_result)
    {
        case CUTE_SUCCESS: status = "\033[1;32m" "pass" "\033[0m"; cute_successes++; break;
        case CUTE_FAILURE: status = "\033[1;31m" "fail" "\033[0m"; cute_failures++; break;
        case CUTE_IGNORED: status = "\033[1;33m" "skip" "\033[0m"; cute_ignores++; break;
        default:           status = "????"; break;
    }

    printf("[%s] %s\n", status, test_name);
    cute_result = CUTE_SUCCESS;
}

static int cute_conclude(const char *file)
{
    struct timeval tock, elapsed;

    suiteTearDown();

    gettimeofday(&tock, NULL);
    timersub(&tock, &cute_tick, &elapsed);

    printf("%d tests %d failed %d ignored in %s (%ld.%03lds)\n",
        cute_successes + cute_failures + cute_ignores,
        cute_failures, cute_ignores, file,
        elapsed.tv_sec, elapsed.tv_usec / 1000);

    return cute_failures == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}


/* --------------- */
/* Internal macros */
/* --------------- */

#define CUTE_IGNORE() do { \
    cute_result = CUTE_IGNORED; \
    return; \
} while(0)

#define CUTE_ASSERT(condition, fmt, ...) do { \
    if ( !(condition) ) { \
        printf("\n   Assertion failed line %d: " fmt "\n\n", __LINE__, __VA_ARGS__); \
        cute_result = CUTE_FAILURE; \
        return; \
    } \
} while (0)

#define CUTE_ASSERT_OP(op, exp, act, conv) CUTE_ASSERT((act) op (exp), "want %s %s " conv " got " conv, #act, #op, exp, act)

#endif /* _CUTE_H_ */
