#include "cvector.h"
#include "cute.h"

/* ------------ */

CVECTOR_GENERATE_HEADER(double)
CVECTOR_GENERATE_SOURCE(double)

/* ------------ */

void setUp(void) {}
void tearDown(void) {}
void suiteSetUp(void) {}
void suiteTearDown(void) {}


void assign_clear_null(void)
{
    TEST_ASSERT_NULL(cvector_assign(double, NULL, 0, 3.14));
    TEST_ASSERT_NULL(cvector_clear(double, NULL));
}

void sub_assign(size_t make, size_t assign, size_t cap)
{
    cvector(double) *ori = cvector_make(double, make);
    cvector(double) *vec = cvector_assign(double, ori, assign, 3.14);

    if (assign <= make)
        TEST_ASSERT_PTR_EQ(ori, vec);

    TEST_ASSERT_ULONG_OP(==, cap, vec->cap);
    TEST_ASSERT_ULONG_OP(==, assign, vec->len);

    for (size_t i = 0; i < assign; i++)
        TEST_ASSERT_DOUBLE_OP(==, 3.14, vec->at[i]);

    cvector_delete(double, vec);
}

void assign_zero_to_zero(void) { sub_assign(0,  0,  0); }
void assign_more_to_zero(void) { sub_assign(0, 42, 42); }
void assign_zero_to_non_zero(void) { sub_assign(10,  0, 10); }
void assign_less_to_non_zero(void) { sub_assign(10,  7, 10); }
void assign_same_to_non_zero(void) { sub_assign(10, 10, 10); }
void assign_more_to_non_zero(void) { sub_assign(10, 13, 20); }
void assign_next_to_non_zero(void) { sub_assign(10, 20, 20); }
void assign_much_to_non_zero(void) { sub_assign(10, 42, 80); }

void sub_clear(size_t make, size_t assign)
{
    cvector(double) *vec;

    if (assign > make) TEST_IGNORE();

    vec = cvector_make(double, make);
    vec = cvector_assign(double, vec, assign, 3.14);
    vec = cvector_clear(double, vec);

    TEST_ASSERT_ULONG_OP(==, make, vec->cap);
    TEST_ASSERT_ULONG_OP(==, 0UL, vec->len);

    cvector_delete(double, vec);
}

void clear_zero_cap(void) { sub_clear( 0, 0); }
void clear_zero_len(void) { sub_clear(10, 0); }
void clear_non_empty(void) { sub_clear(10, 7); }


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(assign_clear_null);

    TEST_RUN(assign_zero_to_zero);
    TEST_RUN(assign_more_to_zero);
    TEST_RUN(assign_zero_to_non_zero);
    TEST_RUN(assign_less_to_non_zero);
    TEST_RUN(assign_same_to_non_zero);
    TEST_RUN(assign_more_to_non_zero);
    TEST_RUN(assign_next_to_non_zero);
    TEST_RUN(assign_much_to_non_zero);

    TEST_RUN(clear_zero_cap);
    TEST_RUN(clear_zero_len);
    TEST_RUN(clear_non_empty);

    return TEST_END();
}
