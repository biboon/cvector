#include "cvector.h"
#include "cute.h"

/* ------------ */

typedef double *double_ptr;
CVECTOR_GENERATE_HEADER(double_ptr)
CVECTOR_GENERATE_SOURCE(double_ptr)

/* ------------ */

void setUp(void) {}
void tearDown(void) {}
void suiteSetUp(void) {}
void suiteTearDown(void) {}


void foreach_null(void)
{
    cvector_foreach(double_ptr, NULL, NULL);
}

void func(double_ptr val)
{
    *val = 1.618;
}

void foreach_test(void)
{
    double array[10];
    cvector(double_ptr) *vec;

    vec = cvector_make(double_ptr, 10);

    for (size_t i = 0; i < vec->cap; i++)
    {
        cvector_push(double_ptr, vec, &array[i]);
        array[i] = 3.14;
    }

    TEST_ASSERT_ULONG_OP(!=, 0UL, vec->len); /* make sure the array is not empty */

    cvector_foreach(double_ptr, vec, func);

    for (size_t i = 0; i < vec->len; i++)
        TEST_ASSERT_DOUBLE_OP(==, 1.618, array[i]);

    cvector_delete(double_ptr, vec);
}


int main(void)
{
    TEST_BEGIN();

    TEST_RUN(foreach_null);
    TEST_RUN(foreach_test);

    return TEST_END();
}
